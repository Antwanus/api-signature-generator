import hashlib
import random
import base64
import urllib
import hmac


secretkey = "F874A96958482AFE"

# Generates a random string of ten digits
salt = str(random.getrandbits(32))
print 'salt', salt

# Computes the signature by hashing the salt with the secret key as the key
signature = hmac.new(secretkey, msg=salt, digestmod=hashlib.sha256).digest()

# base64 encode...
encodedSignature = base64.encodestring(signature).replace('\n', '')

# urlencode...
encodedSignature = urllib.quote(encodedSignature)

print "Voila! A signature: " + encodedSignature
